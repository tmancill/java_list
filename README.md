JAVA class list generator
=========================

JAVA class list generator is a simple script which will check the jar file
contained in a Debian package and generate a list of class that the jar
contains. It will then store the class list in a sqlite database.
The script also has the option to search package if a class name or part
of class name is given.

## Prerequisite

apt-file should have been installed.

## Usage

  $ ./get_list.py -h                 
    Usage:                    
     -h: Display this help message.             
     -u: Update database.                
     -c <class>: Find packages containg class. (Can be part of the class name).             
     -p <package>: List class names and jars contained in the package.

## Detailed Usage

Using `-u` will update the database, or if it is the first run then it will              
create the database in the current folder. keep the script and the db in              
same folder (for now). `-u` will exit immediately asking you to do              
`apt-file update`. If `apt-file update` has been done, use `-u -y`.

`-c` will need classname or part of it and will list packages containing that              
class.

  $ ./get_list.py -c org.eclipse.emf.common              
    eclipse-emf-common-2.19.0.jar in libeclipse-emf-common-java

`-p` with package name will list the jars it contains along with their class.

  $ ./get_list.py -p libeclipse-emf-common-java

## Note

  - First run will take some time as it will need to check all the java              
    packages, currently 1602 and generate the list. From next run, when              
    it will just update it will skip all packages whose version has not              
    changed and will be much faster.

  - The script checks the file:            
    /var/lib/apt/lists/deb.debian.org_debian_dists_unstable_main_binary-amd64_Packages            
    You might need to change the filename based on the mirror that you use.
