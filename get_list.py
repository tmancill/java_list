#!/usr/bin/python3
# ------------------------------------------------------------------------------
#
# Copyright (C) 2020 Sudip Mukherjee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ------------------------------------------------------------------------------

import sqlite3
import subprocess
import os
import sys, getopt
from tempfile import TemporaryDirectory

import signal
import sys

pkgname=""
pkgver=""
jarname=""
classname=""
curdir=""
sigreceived = False

def signal_handler(sig, frame):
	global sigreceived
	sigreceived = True

def initdbname():
	global curdir, dbname
	curdir=os.getcwd()
	dbname=curdir+"/class.db"

def init():
	initdbname()

def printhelp():
			print("""Usage:
 -h: Display this help message.
 -u: Update database.
 -c <class>: Find packages containg class. (Can be part of the class name).
 -p <package>: List class names and jars contained in the package.
 """)

def main(argv):
	toupdate = False
	getclass = False
	getpkg = False
	serach_name = ""
	confupdate = False
	try:
		opts, args = getopt.getopt(argv,"huyc:p:")
	except getopt.GetoptError:
		print("to do")
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			printhelp()
			sys.exit()
		elif opt == '-u':
			toupdate = True
		elif opt == '-y':
			confupdate = True
		elif opt == '-c':
			getclass = True
			search_name = arg
		elif opt == '-p':
			getpkg = True
			search_name = arg

	signal.signal(signal.SIGINT, signal_handler)
	if toupdate == False and getclass == False and getpkg == False:
		printhelp()
		sys.exit()
	elif toupdate == True and confupdate == False:
		print("Please do 'sudo apt-file update' and re-execute the command with -u -y")
		sys.exit()
	elif toupdate == True and confupdate == True:
		init()
		updatedb()
		maintdb()
	elif getclass == True:
		initdbname()
		searchclass(search_name)
	elif getpkg == True:
		initdbname()
		searchpkg(search_name)

def maintdb():
	global dbname
	try:
		conn = sqlite3.connect(dbname)
		conn.execute("VACUUM")
	except:
		print("something went wrong")
	finally:
		conn.close()
	

def searchpkg(pkgnm):
	global dbname
	jarlist = ""

	try:
		con = sqlite3.connect(dbname)
		cursorObj = con.cursor()
		cursorObj.execute("select pkgid from package where pkgname = '" + pkgnm + "'")
		rows = cursorObj.fetchall()
		if len (rows) == 0:
			print("not found")
			return
		pkgid = rows[0][0]
		cursorObj.execute("select jarid, jarname from jar where pkgid = '" + str(pkgid) + "'")
		rows = cursorObj.fetchall()
		if len (rows) == 0:
			print("not found")
			return
		for row in rows:
			cursorObj.execute("select class from class where jarid = '" + str(row[0]) + "'")
			rows1 = cursorObj.fetchall()
			if len (rows1) == 0:
				continue
			print("jar - " + row[1] + " : ")
			for row1 in rows1:
				print("\t" + row1[0])


	except:
		print("something went wrong")
	finally:
		con.close()

def searchclass(classname):
	global dbname
	jarlist = ""

	try:
		con = sqlite3.connect(dbname)
		cursorObj = con.cursor()
		cursorObj.execute("select distinct jarid from class where class like '%" + classname + "%'")
		rows = cursorObj.fetchall()
		if len (rows) == 0:
			print("not found")
			return
		for row in rows:
			jarlist += str(row[0])
			jarlist += ","
		jarlistfinal = jarlist.strip(",")
		cursorObj.execute("select a.pkgname, b.jarname from package as a, jar as b where a.pkgid=b.pkgid and b.jarid in (" + jarlistfinal + ")")
		finalrows = cursorObj.fetchall()
		for row in finalrows:
			print(row[1] + " in " + row[0])

	except:
		print("something went wrong")
	finally:
		con.close()


def updatedb():
	global curdir, sigreceived
	skip = False

	initdb()
	aptlist = subprocess.check_output(["apt-file", "search", "-l", "-x", "/usr/share/java/.+jar$"]).decode().split("\n")
	f = open("/var/lib/apt/lists/deb.debian.org_debian_dists_unstable_main_binary-amd64_Packages", "r")
	for xs in f:
		if sigreceived == True:
			return
		x=xs.strip("\n")
		if (len(x)) == 0:
			skip = False
		if skip == True:
			continue
		if x.startswith("Package: "):		
			a = x.split(" ")
			pkgname = a[1]
			if pkgname in aptlist:
				skip = False
			else:
				skip = True
		if x.startswith("Version: "):
			a = x.split(" ")
			pkgver=a[1]
		if x.startswith("Filename: "):
			a = x.split(" ")
			a1 = a[1].split("/")
			l = len(a1)
			debname = a1[l - 1]
			process(pkgname, pkgver, debname)
	f.close()

def initdb():
	try:
		con = sqlite3.connect(dbname)
		cursorObj = con.cursor()
		cursorObj.execute("create table if not exists package (pkgname text not null unique, version text not null, pkgid integer primary key)")
		cursorObj.execute("create table if not exists jar (jarname text not null, jarid integer primary key, pkgid integer not null, unique (jarname, pkgid),  foreign key(pkgid) references package (pkgid) on delete cascade  on update cascade)")
		cursorObj.execute("create table if not exists class (class text not null, jarid integer not null, unique (class, jarid) foreign key (jarid) references jar(jarid) on delete cascade  on update cascade)")
		con.commit()
	except Error:
		print(Error)
	finally:
		con.close()

def process(pkgn, pkgv, debname):
	with TemporaryDirectory() as tmpdir:
		try:
			os.chdir(tmpdir)
			con = sqlite3.connect(dbname)
			cursorObj = con.cursor()
			cursorObj.execute("select pkgid from package where pkgname='"+ pkgn + "' and version='" + pkgv + "'")
			rows = cursorObj.fetchall()
			if len (rows) != 0:
				con.rollback()
				return
			ret = subprocess.call(["apt-get", "download", pkgn])
			if ret != 0:
				con.rollback()
				return
			debtempname = subprocess.check_output(["ls"]).decode().split("\n")
			ret = subprocess.call(["dpkg-deb", "-x", debtempname[0], "deb"])
			if ret != 0:
				con.rollback()
				return
			cursorObj.execute("replace into package (pkgname, version) values ('" + pkgn + "', '" + pkgv + "')")
			jarnametemp = subprocess.check_output(["find", "deb", "-type", "f", "-name", "*.jar"]).decode().split("\n")
			if (len(jarnametemp) == 1):
				con.commit()
				return
			print("updating: " + pkgn)
			cursorObj.execute("select pkgid from package where pkgname='"+ pkgn + "' and version='" + pkgv + "'")
			rows = cursorObj.fetchall()
			if len (rows) != 1:
				print ("we messed up with " + pkgn)
				con.rollback()
				return
			pkgid = rows[0][0]
			for i in range(len(jarnametemp) - 1):
				jarnamebase = jarnametemp[i].split("/")
				cursorObj.execute("replace into jar (jarname, pkgid) values ('" +  jarnamebase[len(jarnamebase) - 1] + "', " + str(pkgid) + ")")
				cursorObj.execute("select jarid from jar where jarname = '" + jarnamebase[len(jarnamebase) - 1] + "' and pkgid = " + str(pkgid))
				jarrows = cursorObj.fetchall()
				if len (jarrows) != 1:
					print ("we messed up with " + pkgn + " and " + jarnamebase[len(jarnamebase) - 1])
					con.rollback()
					return
				jarid = jarrows[0][0]
				classnametemp = subprocess.check_output(["jar", "-tf", jarnametemp[i]]).decode().split("\n")
				for l in classnametemp:
					if l.endswith(".class"):
						lr = l.replace("/", ".")
						cursorObj.execute("replace into class (class, jarid) values ('" + lr + "', " + str(jarid) + ")")
			con.commit()
		except:
			con.rollback()
			print("something went wrong")
		finally:
			con.close()

if __name__ == "__main__":
	main(sys.argv[1:])
